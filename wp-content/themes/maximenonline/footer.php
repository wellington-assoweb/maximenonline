		</div><!-- /#wrap -->

		<footer class="footer">
			<div class="my-container">
				<div class="row">
					<div class="col-xs-12 col-md-4 calibri_regular">
						<div class="ouvidoria">
							<h3>Ouvidoria</h3>
							<div>Você já é nosso cliente? Tem alguma dúvida?</div>
							<div>Escreva para: <span class="calibri_bold">ouvidoria@apoiourologia.com.br</span></div>
							<div>ou ligue para: <span class="calibri_bold">(48) 3207-1136</span></div>
						</div>
					</div>
					<div class="col-xs-12 col-md-5">
						<div class="logo-footer">
							<img src="<?php echo THEMEURL ?>/assets/img/logo-branca.svg" alt="">
							<hr>
							<div class="calibri_light">DR. ANDRÉ MILANEZI LORENZINI<br/>CRM-MG: 37.233</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-3">
						<div class="podemos-ajudar">
							<h3>Podemos Ajudar?</h3>
							<?php
								wp_nav_menu(
									array(
										'menu'           => 'Footer',
										'theme_location' => 'footer',
										'depth'          => 2,
										'container'      => '',
										'menu_id'        => 'one-header',
										'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
										'walker'         => new wp_bootstrap_navwalker()
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
		</footer><!-- FIM FOOTER -->
		<?php wp_footer();?>
	</body>
</html>