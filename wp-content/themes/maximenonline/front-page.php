<?php /* Template name: Página Inicial */ get_header('home'); ?>
<?php  include(TEMPLATEPATH . '/archive-slide.php'); ?>
<section class="comece-hoje">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-5 centering title-subli">
				<h2><?php echo get_field('titulo_primeira_section'); ?></h2>
			</div>
			<div class="col-xs-12">
				<?php echo get_field('conteudo_primeira_section'); ?>
			</div>
		</div>
	</div>
</section>
<section class="disf-ejac">
	<div class="my-container">
		<div class="row">
			<?php
				while( have_rows('segunda_section') ): the_row();
					// vars
					$titulo = get_sub_field('titulo');
					$imagem = get_sub_field('imagem');
					$conteudo = get_sub_field('conteudo');
			?>
				<div class="col-xs-12 col-sm-6">
					<div class="box">
						<h3><?php echo $titulo ?></h3>
						<img src="<?php echo $imagem['url']; ?>" title="<?php echo $imagem['title']; ?>" alt="<?php echo $imagem['alt']; ?>">
						<div class="box-texto">
							<?php echo $conteudo; ?>
						</div>
						<div class="botao">
							<a href="#">SAIBA MAIS SOBRE A CONDIÇÃO</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<section class="quatro-passos">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php echo get_field('titulo_terceira_section'); ?></h2>
			</div>
			<?php
				while( have_rows('itens') ): the_row();
					// vars
					$titulo = get_sub_field('titulo');
					$imagem = get_sub_field('imagem');
					$conteudo = get_sub_field('conteudo');
			?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="box">
						<img src="<?php echo $imagem['url']; ?>" title="<?php echo $imagem['title']; ?>" alt="<?php echo $imagem['alt']; ?>">
						<h3><?php echo $titulo ?></h3>
						<?php echo $conteudo; ?>
					</div>
				</div>
			<?php endwhile; ?>
			<div class="botao">
				<a href="#">COMECE O TRATAMENTO AQUI</a>
			</div>
		</div>
	</div>
</section>
<section class="nossa-equipe">
	<div class="overlay"></div>
	<div class="box">
		<h2><?php echo get_field('titulo_quarta_section'); ?></h2>
		<?php echo get_field('conteudo_quarta_section'); ?>
	</div>
</section>
<?php get_footer(); ?>