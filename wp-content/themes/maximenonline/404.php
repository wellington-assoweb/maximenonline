<?php get_header(); ?>

<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="the-error">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<div class="search404">
					<h2>Infelizmente, não encontramos essa página.</h2>
					<div class="error">
						<p>Talvez o menu acima tenha o que você procura!</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-3">
				<?php include(TEMPLATEPATH . '/template-parts/sidebar.php'); ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8  col-lg-8">
			</div>
		</div>
	</div><!-- .container -->
</section>

<?php get_footer(); ?>