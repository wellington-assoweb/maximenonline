<?php if (is_front_page()): ?>
	<style>
		<?php
			$args = array( 'post_type' => 'slide', 'order'=> 'ASC', 'posts_per_page' => -1 );
			$loop = new WP_Query( $args );
			 while( $loop->have_posts() ): $loop->the_post();
			 	while( have_rows('slide_home') ): the_row();
					$imagem = get_sub_field('imagem');

					echo '.bg-banner-'.$imagem['id'].'{background-image: url("'.$imagem['url'].'")}';
				endwhile;
			endwhile;
			wp_reset_query();
		?>
	</style>
<?php elseif(is_page(108)): ?>
	<style>
		<?php
			if( have_rows('parceiros') ){
				while( have_rows('parceiros') ){ the_row();
					$imagem = get_sub_field('imagem');
					echo '.parceiros-'.$imagem['id'].'{background-image: url("'.$imagem['url'].'")}';
				};
			};
		?>
	</style>
<?php endif; ?>
