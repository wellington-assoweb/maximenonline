<?php
/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'sample_scripts');
function sample_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, get_template_directory_uri().'/assets/css/');
  define(JS_PATH, get_template_directory_uri().'/assets/js/');

  wp_enqueue_script('jquery');

  /*Fonts*/
  wp_register_style('open-sans', '//fonts.googleapis.com/css?family=Open+Sans:400,400italic', null, null, 'all' );
  wp_enqueue_style('open-sans');
  wp_register_style('fontello', CSS_PATH.'fontello.css', null, null, 'all' );
  wp_enqueue_style('fontello');
  /*Fim Fonts*/

  //Declaração de scripts
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap.min.js', null, null, true );
  wp_register_script('easing', JS_PATH.'jquery.easing.js', null, null, true );

  wp_register_script('smooth-scroll', JS_PATH.'smooth-scroll.js', null, null, true );
  //wp_register_script('dropdown-js', JS_PATH.'hover-dropdown.js', null, null, true );
  wp_register_script('scripts', JS_PATH.'scripts.js', null, null, true );

  wp_enqueue_script('easing');
  wp_enqueue_script('bootstrap_js');
  wp_enqueue_script('smooth-scroll');
  //wp_enqueue_script('dropdown-js');

  //Declaração de estilos
  wp_register_style('bootstrap', CSS_PATH.'bootstrap.min.css');
  wp_register_style('style', CSS_PATH.'style.css', null, null, 'all' );
  wp_register_style('animate-css', CSS_PATH.'animate.css', null, null, 'all' );
  wp_register_style('wordpress-css', CSS_PATH.'wordpress.css', null, null, 'all' );

  //Chamada de estilos
  wp_enqueue_style('bootstrap');
  wp_enqueue_style('animate-css');
  wp_enqueue_style('wordpress-css');
  wp_enqueue_style('style');

  /*MENU*/
  wp_register_script('classie', JS_PATH.'classie.js', null, null, true );
  //wp_register_script('main', JS_PATH.'main.js', null, null, true );
  wp_enqueue_script('classie');

  /*FIM MENU*/

  if(is_front_page()){
    wp_register_style('front-page-css', CSS_PATH.'front-page.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

    wp_register_style('slide-css', CSS_PATH.'slide.css', null, null, 'all' );
    wp_enqueue_style('slide-css');

    wp_register_style('carousel-css', CSS_PATH.'owl.carousel.css', null, null, 'all' );
    wp_enqueue_style('carousel-css');


    wp_register_script('carousel-js', JS_PATH.'owl.carousel.min.js', null, null, true );
    wp_enqueue_script('carousel-js');

    wp_register_script('home-js', JS_PATH.'home.js', null, null, true );
    wp_enqueue_script('home-js');

    wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );
    wp_enqueue_script('wow-js');

  }elseif(is_page(12) || is_page(67) || is_page(101) ){ // EJACULAÇÃO PRECOCE, IMPOTÊNCIA SEXUAL e AMBOS
    wp_register_style('slide-css', CSS_PATH.'slide.css', null, null, 'all' );
    wp_enqueue_style('slide-css');
    wp_register_style('carousel-css', CSS_PATH.'owl.carousel.css', null, null, 'all' );
    wp_enqueue_style('carousel-css');
    wp_register_style('ejacula-e-impoten-css', CSS_PATH.'ejacula-e-impoten.css', null, null, 'all' );
    wp_enqueue_style('ejacula-e-impoten-css');


    wp_register_script('carousel-js', JS_PATH.'owl.carousel.min.js', null, null, true );
    wp_enqueue_script('carousel-js');
    wp_register_script('home-js', JS_PATH.'home.js', null, null, true );
    wp_enqueue_script('home-js');
    wp_register_script('play-pause-js', JS_PATH.'play-pause.js', null, null, true );
    wp_enqueue_script('play-pause-js');

  }elseif(is_page(79) || is_page(92)){ // MEDICO COORDENADOR, TELEMEDICINA
    wp_register_style('medico-telemedicina-css', CSS_PATH.'medico-telemedicina.css', null, null, 'all' );
    wp_enqueue_style('medico-telemedicina-css');

  }elseif(is_page(120)){ // DÚVIDAS
    wp_register_style('duvidas-css', CSS_PATH.'duvidas.css', null, null, 'all' );
    wp_enqueue_style('duvidas-css');

  }elseif(is_page(128) || is_page(130)){ // PAGAMENTO DO TRATAMENTO e AVALIAÇÃO MÉDICA
    wp_register_style('avaliacao-medica-e-pgto-css', CSS_PATH.'avaliacao-medica-e-pgto.css', null, null, 'all' );
    wp_enqueue_style('avaliacao-medica-e-pgto-css');

    wp_register_script('form-js', JS_PATH.'form.js', null, null, true );
    wp_enqueue_script('form-js');
  }elseif(is_home() || is_category() || is_single() || is_author() || is_page(112) || is_page(171)){
    //blog
      wp_register_style('blog-css', CSS_PATH.'blog.css', null, null, 'all' );
      wp_enqueue_style('blog-css');

  }elseif(is_404()){
      wp_register_style('404-css', CSS_PATH.'404.css', null, null, 'all' );
      wp_enqueue_style('404-css');

  }


  //Chamada de scripts
  wp_enqueue_script('scripts');

}

// Aumenta limite de upload
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
function favicon_admin() {
    $favicon_url = get_template_directory_uri() . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .='<!-- Favicon iPhone -->';
    $favicon .='<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}

//FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//DISQUS
function disqus_embed($disqus_shortname) {
    global $post;
    wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
    echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}

//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_filter( 'show_admin_bar', '__return_false' );
add_theme_support('menus');

require_once('wp_bootstrap_navwalker.php');
//require_once('wlaker_dropdown.php');
register_nav_menus( array(
    'menu'     => __('Menu'),
    'footer'     => __('Footer')
) );

add_filter('show_admin_bar', '__return_false');

/*=======================================================================================
ENABLE THUMBS
=======================================================================================*/
add_theme_support('post-thumbnails');

/*=======================================================================================
UNABLE LOGIN SHOW ERRORS
=======================================================================================*/
add_filter('login_errors',create_function('$a', "return null;"));

/*=======================================================================================
REMOVE HEAD VERSION
=======================================================================================*/
remove_action('wp_head', 'wp_generator');

/*=======================================================================================
ENABLE THUMBS
=======================================================================================*/
add_theme_support( 'post-thumbnails' );

/*=======================================================================================
ENABLE EXCERPTS
=======================================================================================*/
add_post_type_support('page', 'excerpt');



/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT
=======================================================================================*/
function the_content_limit($max_char, $more_link_text = '<br/><i class="fa fa-smile-o"></i>', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]>', $content);
    $content = strip_tags($content);

   if (strlen($_GET['']) > 0) {
      echo "<p>";
      echo $content;
      echo "</p>";
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo "<p>";
        echo $content;
        echo '...';
        echo "</p>";
   }
   else {
      echo "<p>";
      echo $content;
      echo "</p>";
   }
}

/*MAIS LIDAS
======================*/
// Verifica se não existe nenhuma função com o nome post_count_session_start
if ( ! function_exists( 'post_count_session_start' ) ) {
    // Cria a função
    function post_count_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', 'post_count_session_start' );
}


// CUSTOM POST TYPE SLIDE
add_action('init', 'type_post_slide');
function type_post_slide() {
  $labels = array(
    'name'                => _x( 'Slide', 'Post Type General Name', 'slide' ),
    'singular_name'       => _x( 'Slide', 'Post Type Singular Name', 'slide' ),
    'menu_name'           => __( 'Slide', 'slide' ),
    'parent_item_colon'   => __( 'Parent Item:', 'Slide' ),
    'all_items'           => __( 'Todos os slides', 'slide' ),
    'view_item'           => __( 'Ver Slide', 'slide' ),
    'add_new_item'        => __( 'Add novo Slide', 'slide' ),
    'add_new'             => __( 'Add novo', 'slide' ),
    'edit_item'           => __( 'Editar Slide', 'slide' ),
    'update_item'         => __( 'Atualizar Slide', 'slide' ),
    'search_items'        => __( 'Pesquisar Slide', 'slide' ),
    'not_found'           => __( 'Nada encontrado', 'slide' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'slide' ),
    );
  $rewrite = array(
    'slug'                => 'slide'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-slides',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'excerpt', 'custom-fields', 'revisions', 'trackbacks')
    );

    register_post_type( 'slide' , $args );
    flush_rewrite_rules();
}

// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {

            // Precisamos da variável $post global para obter o ID do post
            global $post;

            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {

                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;

                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );

                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor

            } // Checa a sessão

        } // is_single
        return;
      }
    add_action( 'get_header', 'tp_count_post_views' );
}

add_post_type_support('post', 'excerpt');

/*=======================================================================================
STYLE FOR LOGIN PAGE
=======================================================================================*/
//Link na tela de login para a página inicial
// Custom WordPress Login Logo
function my_login_logo() {
  echo '
    <style type="text/css">
      html{
        background-color: #000;
      }
      .login #login_error{
        display: none;
      }
      body.login div#login h1 a {
        pointer-events: none;
        background-image: url(wp-content/themes/maximenonline/assets/img/logo.svg);
        padding-bottom: 0px;
        background-size: contain;
        width: 270px;
      }
      body.login {
        background-color:rgba(255, 153, 10, 0.12);
      }
      .login form {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background:#FFF;
        opacity:0.90;
        border-radius:5px;
      }
      .login #backtoblog a, .login #nav a {
        text-decoration: none;
        font-weight:bold;
      }
      .login label {
        color: #0f602c !important;
        text-transform: uppercase;
        font-size: 14px;
      }
      .login input{
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
      .wp-core-ui .button.button-large {
        height: 51px !important;
        line-height: 28px;
        margin-top:15px;
        color: #FFF;
        border-color: transparent !important;
        padding: 0px 12px 2px;
        width: 100%;
        background: #0072bc none repeat scroll 0% 0% !important;
        border-radius: 0px;
        text-transform: uppercase;
      }
      .wp-core-ui .button.button-large:hover{
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
      }
      .login input[type=text]:focus,
      .login input[type=password]:focus{
        -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        -moz-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
      }
    </style>
  ';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );