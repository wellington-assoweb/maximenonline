<?php /* Template name: Telemedicina */ get_header(); ?>
<section class="telemedicina" style="background-image:url('<?php echo THEMEURL ?>/assets/img/telemedicina.jpg')">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="box">
					<h1><?php echo get_field('titulo_da_sessao'); ?></h1>
					<?php echo get_field('conteudo_da_sessao'); ?>
					<div class="botao">
						<a href="<?php echo get_field('link'); ?>"><?php echo get_field('nome_no_botão'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>