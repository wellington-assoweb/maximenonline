<?php /* Template name: Agradecimento */ get_header(); ?>
<section class="pgto">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="title-pers"><?php echo get_field('titulo'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section class="box-subtitle">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php echo get_field('sub_titulo'); ?></h2>
			</div>
		</div>
	</div>
</section>
<section class="medico" style="background-image:url('<?php echo THEMEURL ?>/assets/img/medico-coordenador.jpg')">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="box">
					<h1><?php echo get_field('titulo_da_sessao'); ?></h1>
					<?php echo get_field('conteudo_da_sessao'); ?>
					<div class="botao">
						<a href="<?php echo get_field('link'); ?>"><?php echo get_field('nome_no_botão'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>