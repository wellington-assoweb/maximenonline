<?php /* Template name: Avaliação Médica */ get_header(); ?>
<section class="pgto">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php echo get_field('titulo'); ?></h1>
				<h2><?php echo get_field('sub_titulo'); ?></h2>
				<h3><?php echo get_field('texto_complementar'); ?></h3>
			</div>
			<?php
				while( have_rows('passos') ): the_row();
					// vars
					$titulo = get_sub_field('titulo');
					$imagem = get_sub_field('imagem');
					$conteudo = get_sub_field('conteudo');
			?>
				<div class="col-xs-12 col-sm-4">
					<div class="box">
						<img src="<?php echo $imagem['url']; ?>" title="<?php echo $imagem['title']; ?>" alt="<?php echo $imagem['alt']; ?>">
						<h4><?php echo $titulo ?></h4>
						<?php echo $conteudo; ?>
					</div>
				</div>
			<?php endwhile;	 ?>
		</div>
	</div>
</section>
<section class="formulario">
	<div class="titleForm">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12">
					<h2>Informações Clínicas Pessoais: </h2>
					<h3>Informações com sigilo médico preservado</h3>
				</div>
			</div>
		</div>
	</div>
	<?php  echo do_shortcode('[contact-form-7 id="141" title="Avaliação Médica"]') ?>

	<!-- <div class="form-odd">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">1) Qual problema mais te incomoda?</div>
					<div class="resposta">
					[radio radio-qual-problema-mais-incomoda default:1 "Ejaculação precoce (rápida)" "Disfunção erétil (dificuldade de ereção/impotência)" "Falta de  desejo sexual (a libido)"]<span class="obs">(O tratamento para estes problemas gera a sensação de aumento peniano tanto em comprimento quanto em largura)</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">2) Há quanto tempo isso acontece? Apenas números</div>
					<div class="resposta">
						[text* text-quanto-tempo class:campo-menor]
						Quantidade:
						[checkbox checkbox-quanto-tempo "Meses" "Anos"]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-even">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">3) Quando foi sua última relação sexual com penetração?</div>
					<div class="resposta">
						Apenas números
						[text* text-ultima-relacao class:campo-menor]
						Quantidade:
						[radio radio-ultima-relacao default:1 "Dias" "Meses" "Anos"]
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">4) Apresenta ereções espontâneas? Aquelas involuntárias que temos de noite dormindo, sonhando ou de manhã?</div>
					<div class="resposta">
					[radio radio-apresenta-erecoes default:1 "Sim, e são fortes" "Sim, e são médias" "Sim, e são fracas" "Não apresento mais"]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-odd">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">5) Já esteve em alguma clínica ou médico para tratamento?</div>
					<div class="resposta">
						[radio radio-ja-esteve-em-clinica default:1 "Sim" "Não"]
						Qual?
						[text text-qual-clinica]
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">6) No caso de ter ido a alguma clínica ou médico, ele orientou:</div>
					<div class="resposta">
						[checkbox checkbox-medico-orientou-1 "Procurar um psicólogo." "Tomar comprimidos."][text text-818 placeholder "Quais?"]
						[checkbox checkbox-medico-orientou-2 "Tomar cápsulas."][text text-quais-capsulas placeholder "Quais?"]
						[checkbox checkbox-medico-orientou-3 "Usar spray sublingual." "Aplicar injeções no pênis com aplicador anestésico." "Disse estar normal." "Outra orientações."][text text-quais-orientacoes placeholder "Quais?"]
						[checkbox checkbox-medico-orientou-4 "Nunca fui ao médico para isso."]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-even">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">7) Tem ou já teve algum problema mais sério de saúde como: infarto, derrame cerebral, câncer/problemas na próstata ou cirurgias? Explique:</div>
					<div class="resposta">
						[checkbox checkbox-problema-serio-de-saude "Sim" "Não"]
						Qual?
						[text text-quais-problemas]
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">8) Remédio contínuo para o coração ou para algum outro problema de saúde? Já tomou algum estimulante sexual?</div>
					<div class="resposta">
						[checkbox checkbox-remedio-para-coracao "Sim" "Não"]
						Qual?
						[text text-quais-remedios]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-odd">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">9) Tem alguma lesão peniana visível a olho nu ou palpável?</div>
					<div class="resposta">
						[checkbox checkbox-lesao "Sim" "Não"]
						Qual?
						[text text-quais-lesoes]
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="pergunta">10) Declaro que respondi com veracidade as informações acima e por meio delas a equipe médica fará a avaliação. Meu caso é de urgência médica e desejo ser atendido com rapidez.</div>
					<div class="resposta">
						[checkbox* checkbox-veracidade "Sim"]
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-green">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12">
					<h2>Informações Clínicas Pessoais: </h2>
					<h3>Informações com sigilo médico preservado</h3>
				</div>
				<div class="col-xs-12 col-md-8 centering">
					<p>Temos uma equipe médica para responder a sua avaliação. Caso seu tratamento seja aprovado nossa equipe composta por atendentes do sexo masculino irá entrar em contato. Durante o tratamento você poderá comunicar-se com seu médico através de aplicativo de comunicação instantânea se desejar.</p>
				</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Nome:</div>
				[text* text-nome]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Profissão:</div>
				[text* text-profissao]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso aproximado:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Celular com DDD:</div>
				[text* text-celular]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">E-mail:</div>
				[text* text-email]</div>
			</div>
			<div class="col-xs-12">
				<div class="botao">
					[submit "ENVIAR AVALIAÇÃO"]
				</div>
				<p class="msg-abaixo-botao">Receba e compre seu tratamento preservando ao máximo sua privacidade.</p>
			</div>
		</div>
	</div> -->



</section>
<?php get_footer(); ?>
<script>
	(function($) {

		$('form img.ajax-loader').attr("src", "http://192.168.2.99/maximenonline/wp-content/themes/maximenonline/assets/img/load-form.svg !important");
	})(jQuery);
</script>