<?php /* Template name: Ambos */ get_header(); ?>
<section class="banner">
	<video id="video" src="<?php echo THEMEURL; ?>/assets/video/Clinica-Maximen-video-motivacional" poster="<?php echo THEMEURL ?>/assets/img/capa-video.jpg"></video>
	<div class="overlay"></div>
	<div class="box-play">
		<svg version="1.1" id="buttonPlay" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
			<path id="path" d="M477.7,128C407,5.6,250.4-36.3,128,34.3C5.6,105-36.4,261.6,34.3,384C105,506.4,261.6,548.3,384,477.7
			C506.4,407,548.4,250.4,477.7,128z M358.4,433.3c-97.9,56.6-223.2,23-279.7-74.9c-56.6-97.9-23-223.2,74.9-279.7
			c97.9-56.6,223.2-23,279.7,74.9C489.9,251.6,456.3,376.8,358.4,433.3z M348.1,245.6l-134-78.2c-12.2-7.1-22.1-1.4-22,12.7
			l0.7,155.2c0.1,14.1,10,19.9,22.3,12.8l133-76.8C360.3,264.2,360.4,252.7,348.1,245.6z" fill="#FFFFFF"/>
		</svg>
		<h4>Assistir o vídeo</h4>
	</div>

	<div id="bannerHome" class="owlcarousel">
		<?php
			while( have_rows('slide_ambos') ): the_row();
				// vars
				$titulo = get_sub_field('titulo');
				$sub_titulo = get_sub_field('sub_titulo');
		?>
			<div class="item fullscreen">
				<div class="group-banner">
					<div class="my-container">
						<div class="row">
							<div class="col-xs-12 col-md-7 col-lg-6">
								<h2><?php echo $titulo ?></h2>
								<h3><?php echo $sub_titulo ?></h3>
								<div class="botao">
									<a href="#">COMECE O TRATAMENTO AQUI</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>
<section class="section-white">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?php $imagem = get_field('imagem'); ?>
				<img class="img-section-unico" src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>" title="<?php echo $imagem['title']; ?>">
			</div>
			<div class="col-xs-12 col-md-6">
				<h2><?php echo get_field('titulo'); ?></h2>
				<?php echo get_field('conteudo'); ?>
				<div class="botao">
					<a href="#">COMECE O TRATAMENTO AQUI</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>