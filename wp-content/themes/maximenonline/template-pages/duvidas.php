<?php /* Template name: Duvidas */ get_header(); ?>
<section class="duvidas">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h1>Dúvidas</h1>
				<h2>Tire suas dúvivas com as perguntas mais frequentes</h2>
			</div>
		</div>
	</div>
	<?php
		$count=1;
		while( have_rows('duvidas') ): the_row();
	?>
		<div class="box <?php echo ($count % 2 == 1 ? "box-odd" : "box-even") ?>">
			<div class="my-container">
				<div class="row">
					<div class="col-xs-12">
						<h3><?php echo get_sub_field('pergunta'); ?></h3>
						<?php echo get_sub_field('resposta'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php $count++; endwhile; ?>
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 control-button">
					<div class="botao"><a href="#">COMECE O TRATAMENTO AQUI</a></div>
				</div>
			</div>
		</div>
</section>
<?php get_footer(); ?>