<?php /* Template name: Pgto do Tratamento */ get_header(); ?>
<section class="pgto">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="title-pers"><?php echo get_field('titulo'); ?></h1>
			</div>
		</div>
	</div>
</section>
<section class="box-subtitle">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php echo get_field('sub_titulo'); ?></h2>
			</div>
		</div>
	</div>
</section>
<section class="formulario form-pgto">
	<?php  echo do_shortcode('[contact-form-7 id="149" title="Pagamento do Treinamento"]') ?>

	<!-- <div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 border-right">
				<div class="title-form">
					<h3>Dados do Paciente</h3>
				</div>
				<div class="col-xs-12">
					<div class="pergunta">Você tem algum problema de saúde ou toma algum remédio contínuo?</div>
					<div class="resposta">
					[radio radio-problema-de-saude default:2 "Sim" "Não"]
					[textarea textarea-674 placeholder "Faça um pequeno resumo abaixo sobre o seu problema de saúde e os medicamentos de uso contínuo."]
					</div>
				</div>

				<div class="col-xs-12 col-sm-6"><div class="pergunta">Email:</div>
				[email* email-email]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-Idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>

				<br>
				<div class="title-form">
					<h3>Dados do Envio</h3>
				</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Email:</div>
				[email* email-email]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-Idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Email:</div>
				[email* email-email]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-Idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="title-form">
					<h3>Dados do Pagamento</h3>
				</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Email:</div>
				[email* email-email]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-Idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Email:</div>
				[email* email-email]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Idade:</div>
				[text* text-Idade]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Peso:</div>
				[text* text-peso]</div>
				<div class="col-xs-12 col-sm-6"><div class="pergunta">Altura:</div>
				[text* text-altura]</div>
				<div class="box-green">
					<div  class="title-green">
					<h3>Valor total do seu tratamento: <span class="gadugi_bold">12x de 219,00 <br>Desconto para pagamento a vista.</span></h3></div>
					<p class="gadugi_bold">O tratamento médico Maximen online consiste em:</p>
					<p><b>1-</b>Avaliação e reavaliação médica online por contato telefônico assim como presencial. Receitas médicas dos fármacos prescritas em nome do paciente.</p>
					<p><b>2-</b>Consulta com médico presencial, após concordância de ambas as partes, o mais perto possível de onde se encontra o paciente.</p>
					<p><b>3-</b>Acompanhamento e orientação médica de como fazer o tratamento e de quando retirar os remédios.</p>
					<p><b>4-</b>Envio de Kit completo para tratamento, inclusive para terapia intra-peniana.</p>
					<p><b>5-</b>Acompanhamento médico 24h por dia por telefone ou mensagens de texto (aplicativo What’s up).</p>

					<p class="gadugi_bold">Após o pagamento, seu tratamento chega em até 7 dias uteis.</p>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="botao">
					<a href="#">FAZER TRATAMENTO COMPLETO</a>
				</div>
			</div>
		</div>
	</div> -->




</section>
<?php get_footer(); ?>