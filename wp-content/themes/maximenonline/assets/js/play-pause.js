(function($) {
    jQuery(window).load(function () {
        var video = document.getElementById("video");
        var playButton = document.getElementById("buttonPlay");

    	video.muted = false;
        video.pause();
        playButton.addEventListener("click", function() {
            if (video.paused == true) {
                $('#path').attr('d', 'M179.2,358.4h51.2V153.6h-51.2V358.4z M256,0C115.2,0,0,115.2,0,256s115.2,256,256,256s256-115.2,256-256 S396.8,0,256,0z M256,460.8c-112.6,0-204.8-92.2-204.8-204.8S143.4,51.2,256,51.2S460.8,143.4,460.8,256S368.6,460.8,256,460.8z			 M281.6,358.4h51.2V153.6h-51.2V358.4z').fadeIn();
                video.play();
            	$('#buttonPlay').addClass('disable');
            	$('.box-play').css('opacity', '0.5');
            	$('.box-play').css('transform', 'scale(.5)');
            	$('.box-play h4').html('');
            } else {
            	video.pause();
            	$('#path').attr('d', 'M477.7,128C407,5.6,250.4-36.3,128,34.3C5.6,105-36.4,261.6,34.3,384C105,506.4,261.6,548.3,384,477.7		C506.4,407,548.4,250.4,477.7,128z M358.4,433.3c-97.9,56.6-223.2,23-279.7-74.9c-56.6-97.9-23-223.2,74.9-279.7		c97.9-56.6,223.2-23,279.7,74.9C489.9,251.6,456.3,376.8,358.4,433.3z M348.1,245.6l-134-78.2c-12.2-7.1-22.1-1.4-22,12.7		l0.7,155.2c0.1,14.1,10,19.9,22.3,12.8l133-76.8C360.3,264.2,360.4,252.7,348.1,245.6z').fadeIn();
            	$('#buttonPlay').removeClass('disable');
            	$('.box-play').css('opacity', '1');
            	$('.box-play').css('transform', 'scale(1)');
            }
        });
    });
})(jQuery);