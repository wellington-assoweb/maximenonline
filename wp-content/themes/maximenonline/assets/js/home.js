(function($) {

	jQuery(document).ready(function(){

		$("#bannerHome").owlCarousel({
			// navigation: true,
			// navText: ['<i class="fontello-left-open-big"></i>','<i class="fontello-right-open-big"></i>'],
			nav: true,
			autoHeight: false,
			autoHeight: false,
			items: 1,
			singleItem : false,
			itemsScaleUp : false,
			loop : true,
			autoplay: true,
			autoplayTimeout: 6000
		});

		$('.owl-controls').addClass('my-container');
		$('.owl-dots').addClass('col-xs-12 col-md-7 col-lg-6');
	});


})(jQuery);