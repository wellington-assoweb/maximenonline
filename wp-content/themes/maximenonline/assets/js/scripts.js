(function($) {

    //Hamburger menu addClass for animate
    $('.hamburger-menu').on('click', function() {
        $('.bar').toggleClass('animate');
    })

     //Smaller header when scroll
    $(window).scroll(function () {
        var sc = $(window).scrollTop()
        if (sc > 100) {
            $("#header-sroll").addClass("small")
        }else {
            $("#header-sroll").removeClass("small")
        }
    });


    var tl = 0;
    var magnitude = 0.3;

    addEventListener('scroll', function(){
        var top = $(document).scrollTop();
        interp(top);
    });

    function interp(t){
        var x = t-(tl-t);
        $('.slide').css({top: x*magnitude});
    }


    /* MENU SCRIPT */
    var openbtn = document.getElementById( 'open-button' ),
        headscroll = document.getElementById( 'header-sroll' ),
        isOpen = false;

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener( 'click', toggleMenu );
    }
    function toggleMenu() {
        if( isOpen ) {
            classie.remove( headscroll, 'show-menu' );
        }
        else {
            classie.add( headscroll, 'show-menu' );
        }
        isOpen = !isOpen;
    }
    init();

    function rodape(){
        var footerHeight = $('.footer').height();
        jQuery('.footer').css('margin-top', -(footerHeight)+"px");
        jQuery('.conteudo').css('padding-bottom', (footerHeight + 80)+"px");
    };


    /* Load page Blink Telecom*/
    function loading(){
        $('#loading').css({
            visibility:'hidden',
            opacity: 0
        });
    }
    /* Call some functions when document ready */
    $(document).ready(function($){
        setTimeout(
            function() {
                loading();
            },
        400);
    });


})(jQuery);