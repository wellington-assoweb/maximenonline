
<section class="banner">
	<div id="bannerHome" class="owlcarousel">
		<?php
			$args = array( 'post_type' => 'slide', 'order'=> 'ASC', 'posts_per_page' => -1 );
			$loop = new WP_Query( $args );
			 while( $loop->have_posts() ): $loop->the_post();
			 	while( have_rows('slide_home') ): the_row();
					// vars
					$imagem = get_sub_field('imagem');
					$texto = get_sub_field('texto');
					$texto_do_botão = get_sub_field('texto_do_botão');
					$link = get_sub_field('link');
			?>
				<div class="item fullscreen bg-banner-<?php echo $imagem['id']; ?>">
					<div class="overlay"></div>
					<div class="group-banner">
						<div class="my-container">
							<div class="row">
								<div class="col-xs-12 col-md-7 col-lg-6">
									<h2><?php echo $texto ?></h2>
									<div class="botao">
										<a href="<?php echo $link ?>"><?php echo $texto_do_botão; ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile;
			endwhile;
			wp_reset_query();?>
	</div>
</section>