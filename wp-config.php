<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dbmaximenonline');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`=@@o)s0;O^a+z&k~$yHtP24IDZL``9&r=$eASCfIZ2*RC|g7>3Y>n^OVi$mGug|');
define('SECURE_AUTH_KEY',  'kh]FnXrj4G9,yugp83qTy3a,3<D*8u@/EvtaWOZ^Dz6Y?Qlgng;bx&u%&vpP*-hu');
define('LOGGED_IN_KEY',    'Fq{ZNY%}  c`od$)h|N1!bb4l?5k[i+$gXPY`|=/AQ+_Nj)6|8OK:Viwjv[_-m|2');
define('NONCE_KEY',        'Om4(7M>d}gtfqI9d)NO7KOos-<lrGQ<EPyb46la/yR*Hond9)MsSHVDoxq@%=!0i');
define('AUTH_SALT',        '7zYDjc9W)v%-[gR>- %{do$_6S%?!CF8j<k#+YtsAKmR}r:r{s*&,R{,n3gtO8<%');
define('SECURE_AUTH_SALT', '#]?[7ZF7%,6hrw63lFJ>S$-lP[hNlcy$C<;TfWWR]vlq-GPh2ghmz<TdZ:s2=-d=');
define('LOGGED_IN_SALT',   'Lj79M;N+lE)|_>B|a`at%FCl*1GJQ9u1Z[{A{-Monz+6vJgXW_xFG}JQ=kA*.%Ot');
define('NONCE_SALT',       'r2LFjdS:y) <FVg14mv(;&ND<Y4{S0)|xngTQ]<|-upc(~orz3I8EA@SKI|65,70');

define('THEMEURL',   'http://192.168.2.99/maximenonline/wp-content/themes/maximenonline');
define('SITEURL',       'http://192.168.2.99/maximenonline');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '7s5wfaaz6_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
